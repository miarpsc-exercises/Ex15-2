## Fall 2015 SET

This repository contains materials in preparation for the Fall, **2015
ARRL Simulated Emergency Test**.  Exercise planning is on this
repository's wiki.  The repository contains materials for the
Situation Manual and images, etc. for the wiki and manual.

The fall 2015 exercise is to be held Saturday, September 26 from 8AM
to 1PM local time.
